package com.pku.smart.trade.channel;

import com.pku.smart.log.MyLog;
import com.pku.smart.trade.channel.alipay.AliPayChannelService;
import com.pku.smart.trade.channel.wechatpay.WechatPayChannelService;
import com.pku.smart.trade.exception.BizTradeException;
import com.pku.smart.utils.SpringContextUtil;

/**
 * 支付渠道工厂类
 * 不要使用new来生成对象，否则该对象的属性不能使用@Autowired
 */
public class PayChannelFactory {

    private static final MyLog _log = MyLog.getLog(PayChannelFactory.class);

    private static final Object object = new Object();

    /**
     * 创建支付渠道
     * @param mchId
     * @param channelId
     * @return
     */
    public PayChannelService build(String mchId, String channelId){
        synchronized (object) {
            PayChannelService payChannelService = null;
            if ("WX_NATIVE".equalsIgnoreCase(channelId) || "WX_MICROPAY".equalsIgnoreCase(channelId) || "WX_JSAPI".equalsIgnoreCase(channelId)) {
                payChannelService = SpringContextUtil.getBean(WechatPayChannelService.class);//手动注入
            } else if ("ALIPAY_QR".equalsIgnoreCase(channelId) || "ALIPAY_BR".equalsIgnoreCase(channelId) || "ALIPAY_WAP".equalsIgnoreCase(channelId)) {
                payChannelService = SpringContextUtil.getBean(AliPayChannelService.class);//手动注入
            } else {
                throw new BizTradeException(BizTradeException.PAY_CHANNEL_IS_NOT_EXIST, "无效的渠道编码：" + channelId);
            }
            _log.info("支付渠道构建成功,准备初始化");
            payChannelService.init(mchId, channelId);
            return payChannelService;
        }
    }
}
