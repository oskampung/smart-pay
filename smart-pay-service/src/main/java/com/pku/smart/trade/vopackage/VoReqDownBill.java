package com.pku.smart.trade.vopackage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "VoReqDownBill",description = "账单下载入参")
public class VoReqDownBill implements Serializable {

    @ApiModelProperty(value = "商户编码", required = true, example = "10000000")
    @NotNull(message = "商户编码[mchId]不能为空")
    private String mchId;

    @ApiModelProperty(value = "渠道名称(目前仅支持微信支付宝)", required = true, allowableValues = "ALIPAY,WX", example = "ALIPAY")
    @NotNull(message = "渠道名称[channelName]不能为空")
    private String channelName;

    @ApiModelProperty(value = "对账日期", required = true, example = "2021-02-11")
    @NotNull(message = "对账日期[billDate]不能为空")
    private String billDate;
}
