package com.pku.smart.trade.exception;

import com.pku.smart.exception.BizException;
import com.pku.smart.log.MyLog;

/**
 * 交易模块异常类
 */
public class BizTradeException extends BizException {

    /** 错误的支付方式 **/
    public static final int TRADE_PAY_WAY_ERROR = 102;

    /** 微信异常 **/
    public static final int TRADE_WEIXIN_ERROR = 103;

    /** 订单异常 **/
    public static final int TRADE_ORDER_ERROR = 104;

    /** 交易记录状态不为成功 **/
    public static final int TRADE_ORDER_STATUS_NOT_SUCCESS = 105;

    /** 支付宝异常 **/
    public static final int TRADE_ALIPAY_ERROR = 106;

    /** 参数异常 **/
    public static final int TRADE_PARAM_ERROR = 107;

    /** 请求参数异常 **/
    public static final int REQUEST_PARAM_ERR = 110;

    /** 支付方式不存在 **/
    public static final int PAY_CHANNEL_IS_NOT_EXIST = 109;

    private static final long serialVersionUID = 1;

    private static final MyLog _log = MyLog.getLog(BizTradeException.class);

    public BizTradeException() {
    }

    public BizTradeException(int code, String msgFormat, Object... args) {
        super(code, msgFormat, args);
    }

    public BizTradeException(int code, String msg) {
        super(code, msg);
    }

    public BizTradeException print() {
        _log.info("==>BizTradeException, code:" + this.code + ", msg:" + this.msg);
        return this;
    }
}
