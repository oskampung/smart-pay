package com.pku.smart.trade.service;

import com.pku.smart.trade.vopackage.*;

/**
 * 交易模块管理接口
 */
public interface IPaymentManagerService {

    /**
     * 条码支付，对应支付宝的条码支付或者微信的刷卡支付
     * @param requestVo
     * @return
     */
    VoResTradePayment codePay(VoReqPayCode requestVo);

    /**
     * 扫码支付，用户拿手机扫描二维码
     * @param requestVo
     * @return
     */
    VoResTradePayment scanPay(VoReqPayScan requestVo);

    /**
     * 查询支付订单
     * @param requestVo
     * @return
     */
    VoResTradePayment payQuery(VoReqQueryPay requestVo);

    /**
     * 查询退款订单
     * @param requestVo
     * @return
     */
    VoResTradePayment refundQuery(VoReqQueryRefund requestVo);

    /**
     * 退款
     * @param requestVo
     * @return
     */
    VoResTradePayment refund(VoReqRefund requestVo);

    /**
     * 撤销
     * @param requestVo
     * @return
     */
    VoResTradePayment cancel(VoReqReverse requestVo);

    /**
     * 手机网站支付
     * @param requestVo
     * @return
     */
    VoResTradePayment wapPay(VoReqPayWap requestVo);
}
