package com.pku.smart.trade.channel.alipay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.pku.smart.account.entity.AccountPayChannel;
import com.pku.smart.account.service.IAccountPayChannelService;
import com.pku.smart.ali.config.Configs;
import com.pku.smart.ali.model.result.AlipayF2FPayResult;
import com.pku.smart.ali.service.AlipayTradeService;
import com.pku.smart.ali.service.impl.AlipayTradeServiceImpl;
import com.pku.smart.config.AliPayConfig;
import com.pku.smart.log.MyLog;
import com.pku.smart.trade.channel.PayChannelService;
import com.pku.smart.trade.enums.TradePayRefundStatusEnum;
import com.pku.smart.trade.enums.TradePayStatusEnum;
import com.pku.smart.trade.enums.TradeStateAliPayEnum;
import com.pku.smart.trade.enums.TradeStatusEnum;
import com.pku.smart.trade.service.ITradePayOrderService;
import com.pku.smart.trade.vopackage.*;
import com.pku.smart.utils.AmountUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 支付渠道服务 消除不同渠道误差
 * status SUCCESS:标识调用  tradeStatus标识 支付状态
 */
@Service
public class AliPayChannelService implements PayChannelService {

    private final MyLog _log = MyLog.getLog(AliPayChannelService.class);

    private AlipayClient alipayClient;

    private AlipayTradeService alipayTradeService;

    @Autowired
    AliPayConfig payConfig;

    @Autowired
    IAccountPayChannelService accountPayChannelService;

    @Autowired
    ITradePayOrderService tradePayOrderService;

    /**
     * 初始化配置
     *
     * @param mchId
     * @param channelId
     */
    @Override
    public void init(String mchId, String channelId) {
        _log.info("============开始构建支付宝服务============");

        AccountPayChannel payChannel = accountPayChannelService.getPayChannel(mchId, channelId);
        if (payChannel == null){
            throw new RuntimeException("支付渠道不存在：" + channelId);
        }
        String configParam = payChannel.getParam();
        JSONObject paramObj = JSON.parseObject(configParam);
        payConfig.setIsSandbox("1".equals(paramObj.getString("is_sandbox")));
        String serverUrl = payConfig.getIsSandbox() ? AliPayConfig.URL_DEV : AliPayConfig.URL;
        String appId = paramObj.getString("appid");
        String privateKey = paramObj.getString("private_key");
        String format = AliPayConfig.FORMAT;
        String charset = AliPayConfig.CHARSET;
        String alipayPublicKey = paramObj.getString("alipay_public_key");
        String signType = payConfig.getSignType();
        _log.info("支付宝网关：{}，appId：{}，签名类型：{}，沙箱模式否：{}", serverUrl, appId, signType, payConfig.getIsSandbox());

        AlipayClient client = new DefaultAlipayClient(serverUrl, appId, privateKey, format, charset, alipayPublicKey, signType);
        this.alipayClient = client;

        Configs.setOpenApiDomain(payConfig.getIsSandbox() ? AliPayConfig.URL_DEV : AliPayConfig.URL);
        Configs.setMcloudApiDomain("http://mcloudmonitor.com/gateway.do");
        Configs.setAppid(appId);
        Configs.setPrivateKey(privateKey);
        Configs.setAlipayPublicKey(alipayPublicKey);
        Configs.setSignType(signType);
        Configs.setMaxQueryRetry(5);
        Configs.setQueryDuration(5000);
        Configs.setMaxCancelRetry(3);
        Configs.setCancelDuration(2000);
        Configs.setHeartbeatDelay(5);
        Configs.setHeartbeatDuration(900);
        this.alipayTradeService = new AlipayTradeServiceImpl.ClientBuilder().build();

        _log.info("============构建支付宝服务完毕============");
    }

    /**
     * 条码支付接口
     * 关键字段 tradeStatus-支付状态 channelOrderNo-渠道支付单号
     * 支付状态：1、10000支付成功 2、10003等待用户付款 3、40004支付失败 4、20000未知异常
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject pay(VoReqPayCode requestVo) {
        JSONObject resultMap = new JSONObject();

        //公共返回数据
        resultMap.put("mchId", requestVo.getMchId());
        resultMap.put("channelId", requestVo.getChannelId());
        resultMap.put("mchOrderNo", requestVo.getMchOrderNo());
        resultMap.put("totalAmount", requestVo.getTotalAmount());

        String outTradeNo = requestVo.getMchOrderNo();
        String authCode = requestVo.getAuthCode();
        String subject = requestVo.getSubject();
        String amount = AmountUtil.convertCent2Dollar(String.valueOf(requestVo.getTotalAmount()));//元
        String body = requestVo.getBody();
        String terminalId = requestVo.getDevice();
        String operatorId = requestVo.getMchId();

        try {
            AlipayF2FPayResult payResult = AliPayUtils.tradePay(this.alipayTradeService, outTradeNo, authCode, subject, amount, body, terminalId, operatorId, null);
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
            resultMap.put("errCode", payResult.getResponse().getCode());
            resultMap.put("errMsg", payResult.getResponse().getMsg());
            resultMap.put("tradeStatus", payResult.getResponse().getCode());
            resultMap.put("channelOrderNo","");
            switch (payResult.getTradeStatus()) {
                case SUCCESS:
                    _log.info("支付宝支付成功: )");
                    resultMap.put("channelOrderNo", payResult.getResponse().getTradeNo());
                    break;
                case FAILED:
                    _log.error("支付宝支付失败!!!");
                    break;
                case UNKNOWN:
                    _log.error("系统异常，订单状态未知!!!");
                    break;
                default:
                    _log.error("不支持的交易状态，交易返回异常!!!");
                    break;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            resultMap.put("errCode", e.getErrCode());
            resultMap.put("errMsg", e.getErrMsg());
        } finally {
            _log.info("渠道执行返回：" + JSON.toJSONString(resultMap));
        }

        return resultMap;

//        JSONObject tradeMap = null;
//        try {
//            tradeMap = AliPayUtils.tradePay(this.alipayClient, outTradeNo, authCode, subject, amount, body, terminalId, operatorId, null);
//
//            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
//            resultMap.put("errCode", tradeMap.getString("code"));
//            resultMap.put("errMsg", tradeMap.getString("msg"));
//            resultMap.put("tradeStatus", tradeMap.getString("code"));
//            if ("10000".equals(tradeMap.getString("code"))) {
//                resultMap.put("channelOrderNo", tradeMap.getString("tradeNo"));
//            }
//        } catch (AlipayApiException e) {
//            e.printStackTrace();
//            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
//            resultMap.put("errCode", e.getErrCode());
//            resultMap.put("errMsg", e.getErrMsg());
//        } finally {
//            _log.info("渠道执行返回：" + JSON.toJSONString(resultMap));
//        }
//
//        return resultMap;
    }

    /**
     * 扫码支付
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject precreate(VoReqPayScan requestVo) {
        JSONObject resultMap = new JSONObject();

        //公共返回数据
        resultMap.put("mchId", requestVo.getMchId());
        resultMap.put("channelId", requestVo.getChannelId());
        resultMap.put("mchOrderNo", requestVo.getMchOrderNo());
        resultMap.put("totalAmount", requestVo.getTotalAmount());

        String mchOrderNo = requestVo.getMchOrderNo();
        String subject = requestVo.getSubject();
        String amount = AmountUtil.convertCent2Dollar(String.valueOf(requestVo.getTotalAmount()));//元
        String body = requestVo.getBody();
        String device = requestVo.getDevice();
        String extra = requestVo.getExtra();
        String notifyUrl = payConfig.getNotifyUrl() + "/" + requestVo.getMchId() + "/pay.action";//requestVo.getNotifyUrl();
        _log.info("回调地址：" + notifyUrl);

        JSONObject tradeMap = null;
        try {
            tradeMap = AliPayUtils.tradePrecreate(this.alipayClient, mchOrderNo, subject, amount, body, device, extra, notifyUrl);
            if ("10000".equalsIgnoreCase(tradeMap.getString("code"))) {
                _log.info("调用成功");
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
                resultMap.put("errCode", tradeMap.getString("code"));
                resultMap.put("errMsg", tradeMap.getString("msg"));

                resultMap.put("codeUrl", tradeMap.getString("qrCode"));
            } else {
                _log.info("调用失败");
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                resultMap.put("errCode", tradeMap.getString("subCode"));
                resultMap.put("errMsg", tradeMap.getString("subMsg"));
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            resultMap.put("errCode", e.getErrCode());
            resultMap.put("errMsg", e.getErrMsg());
        } finally {
            _log.info("渠道执行返回：" + JSON.toJSONString(resultMap));
        }

        return resultMap;
    }

    /**
     * 查询支付订单结果(条码支付 扫码支付)
     * 关键字段 tradeStatus-支付状态 channelOrderNo-渠道支付单号
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject query(VoReqQueryPay requestVo) {
        JSONObject resultMap = new JSONObject();

        //公共返回数据
        resultMap.put("mchId", requestVo.getMchId());
        resultMap.put("mchOrderNo", requestVo.getMchOrderNo());
        resultMap.put("channelOrderNo", requestVo.getChannelOrderNo());

        String tradeNo = requestVo.getChannelOrderNo();
        String outTradeNo = requestVo.getMchOrderNo();

        JSONObject tradeMap = null;
        try {
            tradeMap = AliPayUtils.tradeQuery(this.alipayClient, tradeNo, outTradeNo);

            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            if ("10000".equalsIgnoreCase(tradeMap.getString("code"))) {
                String tradeStatus = tradeMap.getString("tradeStatus");
                if (TradeStateAliPayEnum.TRADE_SUCCESS.name().equalsIgnoreCase(tradeStatus)) {
                    _log.info("TRADE_SUCCESS（交易支付成功）");
                    resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
                    resultMap.put("errCode", tradeMap.getString("code"));
                    resultMap.put("errMsg", tradeMap.getString("msg"));

                    resultMap.put("tradeStatus", TradePayStatusEnum.PAY_STATUS_SUCCESS.name());
                    resultMap.put("channelOrderNo", tradeMap.getString("tradeNo"));
                } else if (TradeStateAliPayEnum.TRADE_FINISHED.name().equalsIgnoreCase(tradeStatus)) {
                    _log.info("TRADE_FINISHED（交易结束，不可退款）");
                    resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
                    resultMap.put("errCode", tradeMap.getString("code"));
                    resultMap.put("errMsg", tradeMap.getString("msg"));

                    resultMap.put("tradeStatus", TradePayStatusEnum.PAY_STATUS_SUCCESS.name());
                    resultMap.put("channelOrderNo", tradeMap.getString("tradeNo"));
                } else if (TradeStateAliPayEnum.TRADE_CLOSED.name().equalsIgnoreCase(tradeStatus)) {
                    _log.info("TRADE_CLOSED（未付款交易超时关闭，或支付完成后全额退款）");
                    resultMap.put("errCode", tradeStatus);
                    resultMap.put("errMsg", "未付款交易超时关闭，或支付完成后全额退款");

                    resultMap.put("tradeStatus", TradePayStatusEnum.PAY_STATUS_CLOSED.name());
                } else if (TradeStateAliPayEnum.WAIT_BUYER_PAY.name().equalsIgnoreCase(tradeStatus)) {
                    _log.info("WAIT_BUYER_PAY（交易创建，等待买家付款）");
                    resultMap.put("errCode", tradeStatus);
                    resultMap.put("errMsg", "交易创建，等待买家付款");

                    resultMap.put("tradeStatus", TradePayStatusEnum.PAY_STATUS_PAYING.name());
                    resultMap.put("channelOrderNo", tradeMap.getString("tradeNo"));
                } else {
                    _log.error("未匹配的状态" + tradeStatus);
                    resultMap.put("tradeStatus", TradePayStatusEnum.PAY_STATUS_FAILED.name());
                }
            } else {
                resultMap.put("errCode", tradeMap.getString("subCode"));
                resultMap.put("errMsg", tradeMap.getString("subMsg"));
                resultMap.put("tradeStatus", TradePayStatusEnum.PAY_STATUS_FAILED.name());
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            resultMap.put("errCode", e.getErrCode());
            resultMap.put("errMsg", e.getErrMsg());
        } finally {
            _log.info("渠道执行返回：" + JSON.toJSONString(resultMap));
        }

        return resultMap;
    }

    /**
     * 退款
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject refund(VoReqRefund requestVo) {
        JSONObject resultMap = new JSONObject();

        //公共返回数据
        resultMap.put("mchId", requestVo.getMchId());
        resultMap.put("mchOrderNo", requestVo.getMchOrderNo());
        resultMap.put("channelOrderNo", requestVo.getChannelOrderNo());
        resultMap.put("mchRefundNo", requestVo.getMchRefundNo());
        resultMap.put("refundAmount", requestVo.getRefundAmount());

        String outTradeNo = requestVo.getMchOrderNo();
        String tradeNo = requestVo.getChannelOrderNo();
        String outRequestNo = requestVo.getMchRefundNo();
        String refundAmount = AmountUtil.convertCent2Dollar(String.valueOf(requestVo.getRefundAmount()));//元
        String refundReason = requestVo.getRefundReason();

        JSONObject tradeMap = null;
        try {
            tradeMap = AliPayUtils.tradeRefund(this.alipayClient, tradeNo, outTradeNo, outRequestNo, refundAmount, refundReason);

            if ("10000".equalsIgnoreCase(tradeMap.getString("code"))) {
                if (tradeMap.getBoolean("success")) {
                    resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
                    resultMap.put("errCode", tradeMap.getString("code"));
                    resultMap.put("errMsg", tradeMap.getString("msg"));

                    resultMap.put("channelRefundOrderNo", tradeMap.getString("tradeNo"));
                } else {
                    resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                    resultMap.put("errCode", tradeMap.getString("subCode"));
                    resultMap.put("errMsg", tradeMap.getString("subMsg"));
                }
            } else {
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                resultMap.put("errCode", tradeMap.getString("subCode"));
                resultMap.put("errMsg", tradeMap.getString("subMsg"));
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            resultMap.put("errCode", e.getErrCode());
            resultMap.put("errMsg", e.getErrMsg());
        } finally {
            _log.info("渠道执行返回：" + JSON.toJSONString(resultMap));
        }

        return resultMap;
    }

    /**
     * 退款查询
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject queryRefund(VoReqQueryRefund requestVo) {
        JSONObject resultMap = new JSONObject();

        //公共返回数据
        resultMap.put("mchId", requestVo.getMchId());
        resultMap.put("mchOrderNo", requestVo.getMchRefundNo());
        resultMap.put("channelOrderNo", requestVo.getChannelOrderNo());

        String tradeNo = requestVo.getChannelOrderNo();
        String outTradeNo = requestVo.getMchRefundNo();
        String outRequestNo = requestVo.getMchRefundNo();

        JSONObject tradeMap = null;
        try {
            tradeMap = AliPayUtils.tradeRefundQuery(this.alipayClient, tradeNo, outTradeNo, outRequestNo);

            //调用成功
            if ("10000".equalsIgnoreCase(tradeMap.getString("code"))) {
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
                if (tradeMap.getBoolean("success")) {
                    resultMap.put("errCode", tradeMap.getString("code"));
                    resultMap.put("errMsg", tradeMap.getString("msg"));

                    resultMap.put("tradeStatus", TradePayRefundStatusEnum.REFUND_STATUS_SUCCESS.name());
                } else {
                    resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                    resultMap.put("errCode", tradeMap.getString("subCode"));
                    resultMap.put("errMsg", tradeMap.getString("subMsg"));

                    resultMap.put("tradeStatus", TradePayRefundStatusEnum.REFUND_STATUS_FAILED.name());
                }
            } else {
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                resultMap.put("errCode", tradeMap.getString("subCode"));
                resultMap.put("errMsg", tradeMap.getString("subMsg"));
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            resultMap.put("errCode", e.getErrCode());
            resultMap.put("errMsg", e.getErrMsg());
        } finally {
            _log.info("渠道执行返回：" + JSON.toJSONString(resultMap));
        }

        return resultMap;
    }

    /**
     * 撤销
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject reverse(VoReqReverse requestVo) {
        JSONObject resultMap = new JSONObject();

        //公共返回数据
        resultMap.put("mchId", requestVo.getMchId());
        resultMap.put("mchOrderNo", requestVo.getMchOrderNo());

        String mchOrderNo = requestVo.getMchOrderNo();
        String channelOrderNo = requestVo.getChannelOrderNo();

        JSONObject tradeMap = null;
        try {
            tradeMap = AliPayUtils.tradeCancel(this.alipayClient, channelOrderNo, mchOrderNo);

            if ("10000".equalsIgnoreCase(tradeMap.getString("code"))) {
                if (tradeMap.getBoolean("success")) {
                    resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
                    resultMap.put("errCode", tradeMap.getString("code"));
                    resultMap.put("errMsg", tradeMap.getString("msg"));

                    resultMap.put("channelRefundOrderNo", tradeMap.getString("tradeNo"));
                } else {
                    resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                    resultMap.put("errCode", tradeMap.getString("subCode"));
                    resultMap.put("errMsg", tradeMap.getString("subMsg"));
                }
            } else {
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                resultMap.put("errCode", tradeMap.getString("subCode"));
                resultMap.put("errMsg", tradeMap.getString("subMsg"));
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            resultMap.put("errCode", e.getErrCode());
            resultMap.put("errMsg", e.getErrMsg());
        } finally {
            _log.info("渠道执行返回：" + JSON.toJSONString(resultMap));
        }

        return resultMap;
    }

    /**
     * 手机网站支付
     *
     * @param requestVo
     * @return
     */
    @Override
    public JSONObject wappay(VoReqPayWap requestVo) {
        JSONObject resultMap = new JSONObject();

        //公共返回数据
        resultMap.put("mchId", requestVo.getMchId());
        resultMap.put("channelId", requestVo.getChannelId());
        resultMap.put("mchOrderNo", requestVo.getMchOrderNo());
        resultMap.put("totalAmount", requestVo.getTotalAmount());

        String outTradeNo = requestVo.getMchOrderNo();
        String subject = requestVo.getSubject();
        String totalAmount = AmountUtil.convertCent2Dollar(String.valueOf(requestVo.getTotalAmount()));//元
        String body = requestVo.getBody();
        String quitUrl = requestVo.getQuitUrl();
        String prodCode = "QUICK_WAP_WAY";
        String notifyUrl = payConfig.getNotifyUrl() + "/" + requestVo.getMchId() + "/pay.action";//requestVo.getNotifyUrl();
        _log.info("回调地址：" + notifyUrl);
        String returnUrl = requestVo.getReturnUrl();

        JSONObject tradeMap = null;
        try {
            tradeMap = AliPayUtils.tradeWappay(this.alipayClient, outTradeNo, subject, totalAmount, body, quitUrl, prodCode, notifyUrl, returnUrl);
            if ("10000".equalsIgnoreCase(tradeMap.getString("code"))) {
                _log.info("调用成功");
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
                resultMap.put("errCode", tradeMap.getString("code"));
                resultMap.put("errMsg", tradeMap.getString("msg"));
                resultMap.put("body", tradeMap.getString("body"));
            } else {
                _log.info("调用失败");
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                resultMap.put("errCode", tradeMap.getString("subCode"));
                resultMap.put("errMsg", tradeMap.getString("subMsg"));
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            resultMap.put("errCode", e.getErrCode());
            resultMap.put("errMsg", e.getErrMsg());
        } finally {
            _log.info("渠道执行返回：" + JSON.toJSONString(resultMap));
        }

        return resultMap;
    }

    /**
     * 对账单下载服务
     * @param requestVo 获取对账单地址
     * @return
     */
    @Override
    public JSONObject downbill(VoReqDownBill requestVo) {
        JSONObject resultMap = new JSONObject();

        //公共返回数据
        resultMap.put("mchId", requestVo.getMchId());
        resultMap.put("channelName", requestVo.getChannelName());
        resultMap.put("billDate", requestVo.getBillDate());

        String billDate = requestVo.getBillDate();
        String dateStr = billDate;
        if (billDate.indexOf("-") == -1) {
            dateStr = billDate.substring(0, 4) + "-" + billDate.substring(4, 6) + "-" + billDate.substring(6, 8);
            _log.info("非标准日期格式化为：" + dateStr);
        }

        JSONObject tradeMap = null;
        try {
            tradeMap = AliPayUtils.tradeBillDown(this.alipayClient,dateStr);
            if (!"10000".equalsIgnoreCase(tradeMap.getString("code"))) {
                _log.error("调用失败:" + JSON.toJSONString(tradeMap));
                resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
                resultMap.put("errCode", tradeMap.getString("subCode"));
                resultMap.put("errMsg", tradeMap.getString("subMsg"));
                return resultMap;
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            _log.error("调用异常:" + e.getMessage());
            resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_FAILED.getCode());
            resultMap.put("errCode", e.getErrCode());
            resultMap.put("errMsg", e.getErrMsg());
            return resultMap;
        }

        resultMap.put("retCode", TradeStatusEnum.TRADE_STATUS_SUCCESS.getCode());
        resultMap.put("errCode", tradeMap.getString("code"));
        resultMap.put("errMsg", tradeMap.getString("msg"));
        resultMap.put("billDownloadUrl", tradeMap.getString("billDownloadUrl"));

        return resultMap;
    }

    public AlipayClient getAlipayClient() {
        return alipayClient;
    }

    public void setAlipayClient(AlipayClient alipayClient) {
        this.alipayClient = alipayClient;
    }
}
