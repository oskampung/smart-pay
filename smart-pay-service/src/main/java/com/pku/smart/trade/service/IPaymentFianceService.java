package com.pku.smart.trade.service;

import com.pku.smart.trade.vopackage.VoReqDownBill;
import com.pku.smart.trade.vopackage.VoResTradePayment;

/**
 * 对账业务
 */
public interface IPaymentFianceService {

    /**
     * 对账单下载
     * @param requestVo
     * @return
     */
    VoResTradePayment downBill(VoReqDownBill requestVo);
}
