package com.pku.smart.trade.enums;

import com.pku.smart.constant.PayConstant;

/**
 * 订单退款状态
 */
public enum TradePayRefundStatusEnum {

    /**
     * 未退费
     */
    REFUND_STATUS_INIT(PayConstant.REFUND_STATUS_INIT,"未退费"),

    /**
     * 退费失败
     */
    REFUND_STATUS_FAILED(PayConstant.REFUND_STATUS_FAILED,"退费失败"),

    /**
     * 退费成功
     */
    REFUND_STATUS_SUCCESS(PayConstant.REFUND_STATUS_SUCCESS,"退费成功");

    /** 代码 */
    private Integer code;
    /** 描述 */
    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private TradePayRefundStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static TradePayRefundStatusEnum getObject(Integer code) {
        for (TradePayRefundStatusEnum tradeEnum : TradePayRefundStatusEnum.values()) {
            if (code == tradeEnum.code) {
                return tradeEnum;
            }
        }
        return null;
    }
}
