package com.pku.smart.trade.enums;

import com.pku.smart.constant.PayConstant;

/**
 * 订单支付状态
 */
public enum TradePayStatusEnum {
    /**
     * 交易成功
     */
    PAY_STATUS_SUCCESS(PayConstant.PAY_STATUS_SUCCESS,"交易成功"),

    /**
     * 交易失败
     */
    PAY_STATUS_FAILED(PayConstant.PAY_STATUS_FAILED,"交易失败"),

    /**
     * 订单已创建
     */
    PAY_STATUS_INIT(PayConstant.PAY_STATUS_INIT,"订单已创建"),

    /**
     * 交易关闭
     */
    PAY_STATUS_CLOSED(PayConstant.PAY_STATUS_CLOSED,"交易关闭"),

    /**
     * 订单过期
     */
    PAY_STATUS_EXPIRED(PayConstant.PAY_STATUS_EXPIRED,"订单过期"),

    /**
     * 等待支付
     */
    PAY_STATUS_PAYING(PayConstant.PAY_STATUS_PAYING,"等待支付");

    /** 代码 */
    private Integer code;
    /** 描述 */
    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private TradePayStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static TradePayStatusEnum getObject(Integer code) {
        for (TradePayStatusEnum tradeEnum : TradePayStatusEnum.values()) {
            if (code == tradeEnum.code) {
                return tradeEnum;
            }
        }
        return null;
    }
}
