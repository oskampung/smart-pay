package com.pku.smart.trade.vopackage;

import com.pku.smart.base.VoResBase;
import lombok.Data;

import java.io.Serializable;

@Data
public class VoResPayCode extends VoResBase implements Serializable {
    private String mchId;

    private String channelId;

    private String mchOrderNo;

    private String channelOrderNo;

    private String invokeStatus;
}
