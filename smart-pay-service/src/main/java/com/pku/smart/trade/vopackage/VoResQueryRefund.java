package com.pku.smart.trade.vopackage;

import com.pku.smart.base.VoResBase;
import lombok.Data;

import java.io.Serializable;

@Data
public class VoResQueryRefund extends VoResBase implements Serializable {
    /**
     * 商户编码
     */
    private String mchId;

    /**
     * 渠道编码
     */
    private String channelId;

    /**
     * 商户订单号
     */
    private String mchOrderNo;

    /**
     * 渠道订单号
     */
    private String channelOrderNo;

    /**
     * 商户退款单号
     */
    private String mchRefundNo;

    /**
     * 退款渠道单号
     */
    private String channelRefundNo;

    /**
     * 交易状态
     */
    private String invokeStatus;

}
