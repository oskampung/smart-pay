package com.pku.smart.trade.vopackage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 条码支付入参
 * 必填字段 mchId channelId mchOrderNo totalAmount subject authCode clientIp sign
 */
@Data
@ApiModel(value = "VoReqPayCode",description = "条码支付入参")
public class VoReqPayCode implements Serializable {

    /**
     * 商户编码
     */
    @ApiModelProperty(value = "商户编码", required = true, example = "10000000")
    @NotNull(message = "商户编码[mchId]不能为空")
    private String mchId;

    /**
     * 渠道编码
     */
    @ApiModelProperty(value = "渠道编码", required = true, allowableValues = "ALIPAY_BR,WX_MICROPAY", example = "ALIPAY_BR")
    @NotNull(message = "渠道编码[channelId]不能为空")
    private String channelId;

    /**
     * 渠道商户编码
     */
    //private String channelMchId;

    /**
     * 商户订单号
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "商品订单号", required = true)
    @NotNull(message = "商品订单号[mchOrderNo]不能为空")
    private String mchOrderNo;

    /**
     * 支付金额,单位分
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "支付金额", required = true)
    @NotNull(message = "支付金额[amount]不能为空")
    private Long totalAmount;

    /**
     * 商品标题
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "商品标题", required = true)
    @NotNull(message = "商品标题[subject]不能为空")
    private String subject;

    /**
     * 商品描述信息
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "商品描述信息")
    private String body;

    /**
     * 支付授权码
     * 微信:付款码条形码规则：18位纯数字，以10、11、12、13、14、15开头
     * 支付宝:25~30开头的长度为16~24位的数字，实际字符串长度以开发者获取的付款码长度为准
     */
    @ApiModelProperty(value = "支付授权码", required = true)
    @Size(min = 16 , max = 20 , message = "支付授权码[authCode]长度最小16位最大24位")
    @NotNull(message = "支付授权码[authCode]不能为空")
    private String authCode;

    /**
     * 支付场景 仅支付宝用
     * 条码支付，取值：bar_code
     * 声波支付，取值：wave_code
     */
    @ApiModelProperty(value = "支付场景 仅支付宝用", allowableValues = "bar_code", example = "bar_code")
    private String scene;

    /**
     * 客户端IP
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "订单IP", required = true)
    @Size(min = 1 ,  max = 20 , message = "订单IP[clientIp]长度最小1位，最大20位")
    @NotNull(message = "订单IP[clientIp]不能为空")
    private String clientIp;

    /**
     *
     */
    @ApiModelProperty(value = "设备号")
    private String device;

    /**
     * 特定渠道发起时额外参数
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "特定渠道发起时额外参数")
    private String extra;

    /**
     * 签名
     */
    @ApiModelProperty(value = "签名", required = true)
    @NotNull(message = "签名[sign]不能为空")
    private String sign;
}
