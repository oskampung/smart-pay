package com.pku.smart.trade.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.exception.BizException;
import com.pku.smart.trade.entity.TradePayOrder;
import com.pku.smart.trade.mapper.TradePayOrderMapper;
import com.pku.smart.trade.service.ITradePayOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TradePayOrderServiceImpl implements ITradePayOrderService {

    @Autowired
    TradePayOrderMapper tradePayOrderMapper;

    /**
     * 根据渠道订单号查询
     *
     * @param channelOrderNo
     * @return
     */
    @Override
    public TradePayOrder getTradePayOrder(String channelOrderNo) {
        LambdaQueryWrapper<TradePayOrder> queryWrapper = new LambdaQueryWrapper<TradePayOrder>()
                .eq(TradePayOrder::getChannelOrderNo,channelOrderNo);
        return tradePayOrderMapper.selectOne(queryWrapper);
    }

    /**
     * 根据商户编码和商户订单号查询支付订单
     *
     * @param mchId
     * @param mchOrderNo
     * @return
     */
    @Override
    public TradePayOrder getTradePayOrder(String mchId, String mchOrderNo) {
        LambdaQueryWrapper<TradePayOrder> queryWrapper = new LambdaQueryWrapper<TradePayOrder>()
                .eq(TradePayOrder::getMchId,mchId)
                .eq(TradePayOrder::getMchOrderNo,mchOrderNo);
        return tradePayOrderMapper.selectOne(queryWrapper);
    }

    /**
     * 查询支付订单 商户订单和渠道订单二选一
     *
     * @param mchId
     * @param mchOrderNo
     * @param channelOrderNo
     * @return
     */
    @Override
    public TradePayOrder getTradePayOrder(String mchId, String mchOrderNo, String channelOrderNo) {
        LambdaQueryWrapper<TradePayOrder> queryWrapper = new LambdaQueryWrapper<TradePayOrder>();//QueryWrapper<TradePayOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(TradePayOrder::getMchId,mchId);//queryWrapper.eq("mch_id",mchId);
        if (StringUtils.isNotBlank(mchOrderNo)) {
            queryWrapper.eq(TradePayOrder::getMchOrderNo,mchOrderNo);//queryWrapper.eq("mch_order_no", mchOrderNo);
        }
        if (StringUtils.isNotBlank(channelOrderNo)) {
            queryWrapper.eq(TradePayOrder::getChannelOrderNo,channelOrderNo);//queryWrapper.eq("channel_order_no", channelOrderNo);
        }
        if (StringUtils.isBlank(mchOrderNo) && StringUtils.isBlank(channelOrderNo)){
            throw new BizException("商户订单号和渠道订单号不能同时为空");
        }
        return tradePayOrderMapper.selectOne(queryWrapper);
    }

    /**
     * 插入支付订单
     *
     * @param tradePayOrder
     * @return
     */
    @Override
    public int insertTradePayOrder(TradePayOrder tradePayOrder) {
        tradePayOrder.setCreateTime(new Date());
        tradePayOrder.setCreateBy(PayConstant.TRADE_DEFAULT_OPERA);
        tradePayOrder.setUpdateTime(new Date());
        tradePayOrder.setUpdateBy(PayConstant.TRADE_DEFAULT_OPERA);
        return tradePayOrderMapper.insert(tradePayOrder);
    }

    /**
     * 更新
     *
     * @param tradePayOrder
     * @return
     */
    @Deprecated
    @Override
    public int updateTradePayOrder(TradePayOrder tradePayOrder) {
        tradePayOrder.setUpdateTime(new Date());
        tradePayOrder.setUpdateBy(PayConstant.TRADE_DEFAULT_OPERA);
        return tradePayOrderMapper.updateById(tradePayOrder);
    }

    @Override
    public int updateTradePayOrder(LambdaUpdateWrapper<TradePayOrder> updateWrapper) {
        updateWrapper.set(TradePayOrder::getUpdateTime,new Date());
        updateWrapper.set(TradePayOrder::getUpdateBy,PayConstant.TRADE_DEFAULT_OPERA);
        return tradePayOrderMapper.update(null,updateWrapper);
    }
}
