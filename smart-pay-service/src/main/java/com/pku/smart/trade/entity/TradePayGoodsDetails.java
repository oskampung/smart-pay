package com.pku.smart.trade.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class TradePayGoodsDetails implements Serializable {
    /** 商品ID **/
    private String goodsId;

    /** 名称 **/
    private String goodsName;

    /** 单价 **/
    private Long singlePrice;

    /** 数量 **/
    private Integer nums;

    /** 构造函数 传入所需参数 **/
    public TradePayGoodsDetails (String goodsId , String goodsName ,Long singlePrice , Integer nums){
        this.goodsId = goodsId;
        this.goodsName = goodsName;
        this.singlePrice = singlePrice;
        this.nums = nums;
    }
}
