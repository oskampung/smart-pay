package com.pku.smart.trade.vopackage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class VoReqQueryRefund implements Serializable {

    /**
     * 商户编码
     */
    @ApiModelProperty(value = "商户编码", required = true, example = "10000000")
    @NotNull(message = "商户编码[mchId]不能为空")
    private String mchId;

    /**
     * 商户订单号
     *
     * @mbggenerated
     */
    @ApiModelProperty(value = "商品订单号", required = true)
    @NotNull(message = "商品订单号[mchOrderNo]不能为空")
    private String mchOrderNo;

    /**
     *
     */
    @ApiModelProperty(value = "退款订单号", required = true)
    private String mchRefundNo;

    /**
     * 渠道支付单号
     */
    @ApiModelProperty(value = "渠道支付单号")
    private String channelOrderNo;

    /**
     * 渠道退款单号
     */
    @ApiModelProperty(value = "渠道退款单号")
    private String channelRefundOrderNo;

    /**
     * 签名
     */
    @ApiModelProperty(value = "签名", required = true)
    @NotNull(message = "签名[sign]不能为空")
    private String sign;
}
