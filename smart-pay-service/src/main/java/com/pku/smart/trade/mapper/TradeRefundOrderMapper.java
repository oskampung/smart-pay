package com.pku.smart.trade.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pku.smart.trade.entity.TradeRefundOrder;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface TradeRefundOrderMapper extends BaseMapper<TradeRefundOrder> {
}
