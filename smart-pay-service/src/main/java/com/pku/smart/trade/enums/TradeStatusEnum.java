package com.pku.smart.trade.enums;

import com.pku.smart.constant.PayConstant;

/**
 * 交易态枚举类
 */
public enum TradeStatusEnum {

    TRADE_STATUS_SUCCESS(PayConstant.TRADE_STATUS_SUCCESS,"交易成功"),
    TRADE_STATUS_FAILED(PayConstant.TRADE_STATUS_FAILED,"交易失败"),
    TRADE_STATUS_TIMEOUT(PayConstant.TRADE_STATUS_TIMEOUT,"交易超时");

    /** 代码 */
    private String code;
    /** 描述 */
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private TradeStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static TradeStatusEnum getObject(String code) {
        for (TradeStatusEnum tradeEnum : TradeStatusEnum.values()) {
            if (code == tradeEnum.code) {
                return tradeEnum;
            }
        }
        return null;
    }
}
