package com.pku.smart.trade.vopackage;

import com.pku.smart.constant.PayConstant;
import lombok.Data;

import java.io.Serializable;

@Data
public class VoResTradePayment<T> implements Serializable {

    /**
     * 失败错误码
     */
    private String retCode;

    /**
     * 返回结果
     */
    private T retData;

    public VoResTradePayment() {
        this.setRetCode(PayConstant.TRADE_STATUS_FAILED);
    }
}
