package com.pku.smart.fiance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pku.smart.fiance.entity.FianceAl;
import com.pku.smart.fiance.mapper.FianceAlMapper;
import com.pku.smart.fiance.service.IFianceAlService;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class FianceAlServiceImpl extends ServiceImpl<FianceAlMapper, FianceAl> implements IFianceAlService {

    @Override
    public void deleteAl(String mchId, Date deleteDate) {
        LambdaQueryWrapper<FianceAl> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FianceAl::getMchId,mchId);
        queryWrapper.eq(FianceAl::getDate,deleteDate);
        remove(queryWrapper);
    }
}
