package com.pku.smart.fiance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pku.smart.fiance.entity.FianceWx;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface FianceWxMapper extends BaseMapper<FianceWx> {
}
