package com.pku.smart.fiance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pku.smart.fiance.entity.FianceAl;

import java.util.Date;

public interface IFianceAlService extends IService<FianceAl> {

    void deleteAl(String mchId, Date deleteDate);
}
