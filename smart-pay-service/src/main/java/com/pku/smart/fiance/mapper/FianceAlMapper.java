package com.pku.smart.fiance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pku.smart.fiance.entity.FianceAl;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface FianceAlMapper extends BaseMapper<FianceAl> {
}
