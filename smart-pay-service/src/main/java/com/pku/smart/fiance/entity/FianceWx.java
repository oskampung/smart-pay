package com.pku.smart.fiance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pku.smart.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 微信对账单
 */
@TableName(value = "pm_fiance_wx")
public class FianceWx extends BaseEntity implements Serializable {

    @TableField(value = "wxorder")
    private String wxorder;

    @TableField(value = "bzorder")
    private String bzorder;

    @TableField(value = "tradestatus")
    private String tradestatus;

    @TableField(value = "time")
    private String time;

    @TableField(value = "ghid")
    private String ghid;

    @TableField(value = "merno")
    private String merno;

    @TableField(value = "mchid")
    private String mchid;

    @TableField(value = "submch")
    private String submch;

    @TableField(value = "deviceid")
    private String deviceid;

    @TableField(value = "openid")
    private String openid;

    @TableField(value = "tradetype")
    private String tradetype;

    @TableField(value = "bank")
    private String bank;

    @TableField(value = "currency")
    private String currency;

    @TableField(value = "totalmoney")
    private BigDecimal totalmoney;

    @TableField(value = "redpacketmoney")
    private BigDecimal redpacketmoney;

    @TableField(value = "wxrefundorder")
    private String wxrefundorder;

    @TableField(value = "bzrefundorder")
    private String bzrefundorder;

    @TableField(value = "refundmoney")
    private BigDecimal refundmoney;

    @TableField(value = "redpacketrefundmoney")
    private BigDecimal redpacketrefundmoney;

    @TableField(value = "refundtype")
    private String refundtype;

    @TableField(value = "refundstatus")
    private String refundstatus;

    @TableField(value = "commodityname")
    private String commodityname;

    @TableField(value = "datapacket")
    private String datapacket;

    @TableField(value = "fee")
    private BigDecimal fee;

    @TableField(value = "rate")
    private String rate;

    @TableField(value = "bill_date")
    private Date date;//对账日期

    @TableField(value = "mch_id")
    private String mchId;

    /**
     * 渠道名称,如:alipay,wechat
     *
     * @mbggenerated
     */
    @TableField(value = "channel_name")
    private String channelName;

    /**主键*/
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    public String getWxorder() {
        return wxorder;
    }

    public void setWxorder(String wxorder) {
        this.wxorder = wxorder;
    }

    public String getBzorder() {
        return bzorder;
    }

    public void setBzorder(String bzorder) {
        this.bzorder = bzorder;
    }

    public String getTradestatus() {
        return tradestatus;
    }

    public void setTradestatus(String tradestatus) {
        this.tradestatus = tradestatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGhid() {
        return ghid;
    }

    public void setGhid(String ghid) {
        this.ghid = ghid;
    }

    public String getMerno() {
        return merno;
    }

    public void setMerno(String merno) {
        this.merno = merno;
    }

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getSubmch() {
        return submch;
    }

    public void setSubmch(String submch) {
        this.submch = submch;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getTradetype() {
        return tradetype;
    }

    public void setTradetype(String tradetype) {
        this.tradetype = tradetype;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getTotalmoney() {
        return totalmoney;
    }

    public void setTotalmoney(BigDecimal totalmoney) {
        this.totalmoney = totalmoney;
    }

    public BigDecimal getRedpacketmoney() {
        return redpacketmoney;
    }

    public void setRedpacketmoney(BigDecimal redpacketmoney) {
        this.redpacketmoney = redpacketmoney;
    }

    public String getWxrefundorder() {
        return wxrefundorder;
    }

    public void setWxrefundorder(String wxrefundorder) {
        this.wxrefundorder = wxrefundorder;
    }

    public String getBzrefundorder() {
        return bzrefundorder;
    }

    public void setBzrefundorder(String bzrefundorder) {
        this.bzrefundorder = bzrefundorder;
    }

    public BigDecimal getRefundmoney() {
        return refundmoney;
    }

    public void setRefundmoney(BigDecimal refundmoney) {
        this.refundmoney = refundmoney;
    }

    public BigDecimal getRedpacketrefundmoney() {
        return redpacketrefundmoney;
    }

    public void setRedpacketrefundmoney(BigDecimal redpacketrefundmoney) {
        this.redpacketrefundmoney = redpacketrefundmoney;
    }

    public String getRefundtype() {
        return refundtype;
    }

    public void setRefundtype(String refundtype) {
        this.refundtype = refundtype;
    }

    public String getRefundstatus() {
        return refundstatus;
    }

    public void setRefundstatus(String refundstatus) {
        this.refundstatus = refundstatus;
    }

    public String getCommodityname() {
        return commodityname;
    }

    public void setCommodityname(String commodityname) {
        this.commodityname = commodityname;
    }

    public String getDatapacket() {
        return datapacket;
    }

    public void setDatapacket(String datapacket) {
        this.datapacket = datapacket;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
