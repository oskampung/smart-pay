package com.pku.smart.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pku.smart.account.entity.AccountMchInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface AccountMchInfoMapper extends BaseMapper<AccountMchInfo> {
}
