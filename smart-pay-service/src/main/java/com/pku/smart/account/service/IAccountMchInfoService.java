package com.pku.smart.account.service;

import com.pku.smart.account.entity.AccountMchInfo;

public interface IAccountMchInfoService {

    /**
     * 查询商户信息
     * @param mchId
     * @return
     */
    AccountMchInfo getMchInfo(String mchId);
}
