package com.pku.smart.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.pku.smart.account.entity.AccountPayChannel;
import com.pku.smart.account.mapper.AccountPayChannelMapper;
import com.pku.smart.account.service.IAccountPayChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountPayChannelServiceImpl implements IAccountPayChannelService {

    @Autowired
    AccountPayChannelMapper accountPayChannelMapper;

    /**
     * 根据渠道编码查询支付渠道
     *
     * @param channelId
     * @return
     */
    @Override
    public AccountPayChannel getPayChannel(String mchId, String channelId) {
        LambdaQueryWrapper<AccountPayChannel> queryWrapper = new LambdaQueryWrapper<AccountPayChannel>()
                .eq(AccountPayChannel::getMchId,mchId)
                .eq(AccountPayChannel::getChannelId,channelId);
        AccountPayChannel channel = accountPayChannelMapper.selectOne(queryWrapper);
        return channel;
    }

    /**
     * 根据渠道名称查询支付渠道列表
     * @param mchId
     * @param channelName
     * @return
     */
    @Override
    public List<AccountPayChannel> getPayChannelList(String mchId, String channelName) {
        LambdaQueryWrapper<AccountPayChannel> queryWrapper = new LambdaQueryWrapper<AccountPayChannel>()
                .eq(AccountPayChannel::getMchId,mchId)
                .eq(AccountPayChannel::getChannelName,channelName);
        List<AccountPayChannel> channelList = accountPayChannelMapper.selectList(queryWrapper);
        return channelList;
    }

    /**
     * 根据商户编号查询支付渠道
     *
     * @param mchId
     * @return
     */
    @Override
    public List<AccountPayChannel> getPayChannelList(String mchId) {
        LambdaQueryWrapper<AccountPayChannel> queryWrapper = new LambdaQueryWrapper<AccountPayChannel>()
                .eq(AccountPayChannel::getMchId,mchId);
        List<AccountPayChannel> channelList = accountPayChannelMapper.selectList(queryWrapper);
        return channelList;
    }
}
