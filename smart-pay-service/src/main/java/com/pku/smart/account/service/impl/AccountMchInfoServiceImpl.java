package com.pku.smart.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.pku.smart.account.entity.AccountMchInfo;
import com.pku.smart.account.mapper.AccountMchInfoMapper;
import com.pku.smart.account.service.IAccountMchInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountMchInfoServiceImpl implements IAccountMchInfoService {

    @Autowired
    AccountMchInfoMapper accountMchInfoMapper;

    /**
     * 查询商户信息
     *
     * @param mchId
     * @return
     */
    @Override
    public AccountMchInfo getMchInfo(String mchId) {
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper<AccountMchInfo>().eq(AccountMchInfo::getMchId,mchId);
        return accountMchInfoMapper.selectOne(queryWrapper);
        //return accountMchInfoMapper.selectById(mchId);
    }
}
