package com.pku.smart.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pku.smart.account.entity.AccountPayChannel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface AccountPayChannelMapper extends BaseMapper<AccountPayChannel> {
}
