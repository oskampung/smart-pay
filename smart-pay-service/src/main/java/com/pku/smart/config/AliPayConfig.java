package com.pku.smart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix="config.ali")
public class AliPayConfig {
    // 编码
    public static String CHARSET = "UTF-8";

    // 返回格式
    public static String FORMAT = "json";

    // RSA2
    public static String SIGNTYPE = "RSA2";

    // 请求网关地址
    public static String URL = "https://openapi.alipay.com/gateway.do";

    // 请求网关地址 沙箱
    public static String URL_DEV = "https://openapi.alipaydev.com/gateway.do";

    private String notifyUrl;

    private String signType;

    private Boolean isSandbox = true;// 是否沙箱环境,1:沙箱,0:正式环境

    private String billPath;

    // 商户appid
    //private String app_id;

    // 私钥 pkcs8格式的
    //private String rsa_private_key;

    // 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //private String notify_url;

    // 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
    //private String return_url;



    //密钥类型
    //private String sign_type;

    // 支付宝公钥
    //private String alipay_public_key;

    // 是否沙箱环境,1:沙箱,0:正式环境
    //private Short is_sandbox = 0;

    //对账单下载目录
    //private String bill_path;

    //退款标志 1、运行部分退 0、不允许部分退
    //private String pay_back_flag;



//    public AliPayConfig init(String configParam) {
//        Assert.notNull(configParam, "init alipay config error");
//        JSONObject paramObj = JSON.parseObject(configParam);
//        this.setApp_id(paramObj.getString("appid"));
//        this.setRsa_private_key(paramObj.getString("private_key"));
//        this.setAlipay_public_key(paramObj.getString("alipay_public_key"));
//        this.setIs_sandbox(paramObj.getShortValue("is_sandbox"));
//        this.setSign_type(paramObj.getString("sign_type"));
//        if(this.getIs_sandbox() == 1) {
//            this.setUrl("https://openapi.alipaydev.com/gateway.do");
//        } else {
//            this.setUrl("https://openapi.alipay.com/gateway.do");
//        }
//        if (StringUtils.isBlank(this.sign_type)){
//            this.sign_type = SIGNTYPE;
//        }
//        return this;
//    }
}
