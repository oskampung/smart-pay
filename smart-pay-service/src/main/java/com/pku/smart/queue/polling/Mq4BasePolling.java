package com.pku.smart.queue.polling;

import com.pku.smart.log.MyLog;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

public class Mq4BasePolling {

    private static final MyLog _log = MyLog.getLog(Mq4BasePolling.class);

    @Autowired
    RabbitTemplate rabbitTemplate;

    public void send(final Object msg, final Integer delay, String exchange, String routingKey) {
        //第一个参数是前面RabbitMqConfig的交换机名称 第二个参数的路由名称 第三个参数是传递的参数 第四个参数是配置属性
        rabbitTemplate.convertAndSend(
                exchange,
                routingKey,
                msg,
            message -> {
                //配置消息的过期时间
                message.getMessageProperties().setDelay(delay);
                return message;
            }
        );
    }

//    @Autowired
//    Queue payPollingQueue;
//
//    @Autowired
//    Queue refundPollingQueue;
//
//    @Autowired
//    private JmsTemplate jmsTemplate;
//
//    public void send(final String msg, final long delay, Queue queue) {
//        _log.info("发送MQ延时消息:msg={},delay={}", msg, delay);
//        jmsTemplate.send(this.payPollingQueue, new MessageCreator() {
//            public Message createMessage(Session session) throws JMSException {
//                TextMessage tm = session.createTextMessage(msg);
//                tm.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, delay);//延迟投递的时间
//                tm.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_PERIOD, 1*1000);//重复投递的时间间隔
//                tm.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_REPEAT, 1);//重复投递次数
//                return tm;
//            }
//        });
//    }
}
