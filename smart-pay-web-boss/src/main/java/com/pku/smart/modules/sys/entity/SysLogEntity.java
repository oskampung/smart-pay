/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.pku.smart.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统日志
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
@TableName("sys_log")
public class SysLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 用户名
	 */
	@TableField(value = "username")
	private String username;
	/**
	 * 用户操作
	 */
	@TableField(value = "operation")
	private String operation;
	/**
	 * 请求方法
	 */
	@TableField(value = "method")
	private String method;
	/**
	 * 请求参数
	 */
	@TableField(value = "params")
	private String params;
	/**
	 * 执行时长(毫秒)
	 */
	@TableField(value = "time")
	private Long time;
	/**
	 * IP地址
	 */
	@TableField(value = "ip")
	private String ip;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(value = "name")
	private Date createDate;
}
