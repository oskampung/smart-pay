package com.pku.smart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartPayWebBossApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartPayWebBossApplication.class, args);
    }

}
