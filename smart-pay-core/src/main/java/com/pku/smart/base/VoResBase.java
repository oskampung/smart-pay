package com.pku.smart.base;

import lombok.Data;

@Data
public class VoResBase {
    /**
     * 错误码 如 SYSTEMERROR
     */
    private String errCode;

    /**
     * 结果信息描述 如 微信支付内部错误
     */
    private String errCodeDes;
}
