package com.pku.smart.vopackage;

import com.pku.smart.base.BaseResult;
import lombok.Data;

/**
 * 交易返回结果封装
 */
@Data
public class TradeResultVo extends BaseResult {

    private String mchId;

    private String mchOrderNo;

    private String mchRefundNo;

    private String channelOrderNo;

    private String channelRefundNo;

    private String codeUrl;//仅扫码支付有效

    private String prepayId;//预交易会话标识 JSAPI支付用

    private String invokeStatus;//仅条码支付 支付查询有效

    private String totalAmount;//元

    private String totalCount;//账单记录数

    private String billDate;//账单日期
}
