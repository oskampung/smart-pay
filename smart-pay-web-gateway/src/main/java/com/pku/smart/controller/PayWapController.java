package com.pku.smart.controller;

import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicResponseParameters;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.service.SmartPayService;
import com.pku.smart.trade.service.IPaymentManagerService;
import com.pku.smart.trade.vopackage.VoReqPayWap;
import com.pku.smart.trade.vopackage.VoResPayWap;
import com.pku.smart.trade.vopackage.VoResTradePayment;
import com.pku.smart.vopackage.TradeResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 手机网站支付
 */
@ApiSupport(order = 11)
@Api(tags = "支付网关")
@RestController
@RequestMapping(value = "/api")
public class PayWapController {
    private static final MyLog _log = MyLog.getLog(PayWapController.class);

    @Autowired
    SmartPayService smartPayService;

    @Autowired
    IPaymentManagerService paymentManagerService;

    @ApiOperationSupport(order = 1103)
    @ApiOperation(value = "1103 网页支付")
    @DynamicResponseParameters(properties = {
            @DynamicParameter(name = "retCode",value = "通讯码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCode",value = "错误码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCodeDes",value = "结果信息描述", example = "交易成功", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchId",value = "商户号", example = "10000000", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchOrderNo",value = "商户号订单号", example = "P2010203029245", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "codeUrl",value = "支付二维码(支付宝)", example = "wxpay://weefg.cv", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "codeUrl",value = "预交易会话标识(微信)", example = "u38isr34lrsd84", required = true, dataTypeClass = String.class)
    })
    @RequestMapping(value = "/pay/pay_wap", method = RequestMethod.POST)
    public String wapPay(@ModelAttribute VoReqPayWap requestVo, BindingResult bindingResult) {
        _log.info("###### 开始接收商户统一条码支付请求 ######");
        TradeResultVo tradeResultVo = new TradeResultVo();
        //公共返回
        tradeResultVo.setMchId(requestVo.getMchId());
        tradeResultVo.setMchOrderNo(requestVo.getMchOrderNo());
        tradeResultVo.setChannelOrderNo("");
        try {
            _log.info("打印：" + JSON.toJSONString(requestVo));
            //参数校验
            smartPayService.validateParams(requestVo, bindingResult);
            //调用服务
            VoResTradePayment<VoResPayWap> payResultVo = paymentManagerService.wapPay(requestVo);
            //结果转换
            tradeResultVo.setRetCode(payResultVo.getRetCode());
            //默认值
            VoResPayWap resPayWap = payResultVo.getRetData();
            _log.info(">>>>>>>>>>>>返回：" + JSON.toJSONString(resPayWap));
            //公共渠道状态
            tradeResultVo.setErrCode(resPayWap.getErrCode());
            tradeResultVo.setErrCodeDes(resPayWap.getErrCodeDes());

            tradeResultVo.setErrCode(resPayWap.getErrCode());
            tradeResultVo.setErrCodeDes(resPayWap.getErrCodeDes());
            tradeResultVo.setCodeUrl(resPayWap.getCodeUrl());//支付宝
            tradeResultVo.setPrepayId(resPayWap.getPrepayId());//微信
        } catch (Exception e) {
            e.printStackTrace();
            tradeResultVo.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResultVo.setErrCode(PayConstant.TRADE_STATUS_SYSTEM_ERROR_CODE);
            tradeResultVo.setErrCodeDes("系统异常：" + e.getMessage());
            _log.error(PayConstant.TRADE_STATUS_SYSTEM_ERROR_DESC, e);
        }

        String result = JSON.toJSONString(tradeResultVo);
        _log.info("打印结果：" + result);

        return result;
    }
}
