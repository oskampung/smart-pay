package com.pku.smart.controller;

import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicResponseParameters;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.service.SmartPayService;
import com.pku.smart.trade.service.IPaymentManagerService;
import com.pku.smart.trade.vopackage.VoReqPayCode;
import com.pku.smart.trade.vopackage.VoResPayCode;
import com.pku.smart.trade.vopackage.VoResTradePayment;
import com.pku.smart.utils.AmountUtil;
import com.pku.smart.vopackage.TradeResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 条码支付 刷卡支付
 * 商户通过扫码设备扫描用户手机
 */
@ApiSupport(order = 11)
@Api(tags = "支付网关")
@RestController
@RequestMapping(value = "/api")
public class PayCodeController {

    private static final MyLog _log = MyLog.getLog(PayCodeController.class);

    @Autowired
    SmartPayService smartPayService;

    @Autowired
    IPaymentManagerService paymentManagerService;

    /**
     * 条码支付
     *
     * @param requestVo     mchId channelId mchOrderNo totalAmount subject body authCode scene clientIp device extra sign
     * @param bindingResult 校验必填字段 mchId channelId mchOrderNo totalAmount subject authCode clientIp sign
     * @return 统一返回retCode errCode errCodeDes  支付成功：加mchId mchOrderNo channelOrderNo
     */
    @ApiOperationSupport(order = 1101)
    @ApiOperation(value = "1101 条码支付")
    @DynamicResponseParameters(properties = {
            @DynamicParameter(name = "retCode",value = "通讯码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCode",value = "错误码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCodeDes",value = "结果信息描述", example = "交易成功", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchId",value = "商户号", example = "10000000", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchOrderNo",value = "商户号订单号", example = "P2010203029245", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "channelOrderNo",value = "渠道订单号", example = "102348934385486857", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "totalAmount",value = "支付金额", example = "1.21", required = true),
            @DynamicParameter(name = "invokeStatus",value = "支付状态", example = "1", required = true, dataTypeClass = String.class)
    })
    @RequestMapping(value = "/pay/pay_order", method = RequestMethod.POST)
    public String codePay(@ModelAttribute VoReqPayCode requestVo, BindingResult bindingResult) {
        _log.info("###### 开始接收商户统一条码支付请求 ######");
        TradeResultVo tradeResultVo = new TradeResultVo();
        //公共返回
        tradeResultVo.setMchId(requestVo.getMchId());
        tradeResultVo.setMchOrderNo(requestVo.getMchOrderNo());
        tradeResultVo.setChannelOrderNo("");

        try {
            //参数校验
            smartPayService.validateParams(requestVo, bindingResult);
            //调用服务
            VoResTradePayment<VoResPayCode> payResultVo = paymentManagerService.codePay(requestVo);
            //结果转换
            tradeResultVo.setRetCode(payResultVo.getRetCode());
            //默认值
            VoResPayCode resPayCode = payResultVo.getRetData();
            _log.info(">>>>>>>>>>>>返回：" + JSON.toJSONString(resPayCode));
            //公共渠道状态
            tradeResultVo.setErrCode(resPayCode.getErrCode());
            tradeResultVo.setErrCodeDes(resPayCode.getErrCodeDes());

            tradeResultVo.setErrCode(resPayCode.getErrCode());
            tradeResultVo.setErrCodeDes(resPayCode.getErrCodeDes());
            tradeResultVo.setInvokeStatus(resPayCode.getInvokeStatus());
            //支付不是直接返回成功
            if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(payResultVo.getRetCode())) {
                tradeResultVo.setMchId(resPayCode.getMchId());
                tradeResultVo.setMchOrderNo(resPayCode.getMchOrderNo());
                tradeResultVo.setChannelOrderNo(resPayCode.getChannelOrderNo());
                tradeResultVo.setTotalAmount(AmountUtil.convertCent2Dollar(requestVo.getTotalAmount().toString()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            tradeResultVo.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResultVo.setErrCode(PayConstant.TRADE_STATUS_SYSTEM_ERROR_CODE);
            tradeResultVo.setErrCodeDes("系统异常：" + e.getMessage());
            _log.error(PayConstant.TRADE_STATUS_SYSTEM_ERROR_DESC, e);
        }

        String result = JSON.toJSONString(tradeResultVo);
        _log.info("打印结果：" + result);

        return result;
    }

}
