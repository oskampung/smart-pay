package com.pku.smart.controller;

import com.alibaba.fastjson.JSON;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicResponseParameters;
import com.pku.smart.constant.PayConstant;
import com.pku.smart.log.MyLog;
import com.pku.smart.service.SmartPayService;
import com.pku.smart.trade.service.IPaymentManagerService;
import com.pku.smart.trade.vopackage.VoReqRefund;
import com.pku.smart.trade.vopackage.VoResRefund;
import com.pku.smart.trade.vopackage.VoResTradePayment;
import com.pku.smart.vopackage.TradeResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@ApiSupport(order = 11)
@Api(tags = "支付网关")
@RestController
@RequestMapping(value = "/api")
public class PayRefundController {

    private static final MyLog _log = MyLog.getLog(PayRefundController.class);

    @Autowired
    SmartPayService smartPayService;

    @Autowired
    IPaymentManagerService paymentManagerService;

    /**
     * 订单退费
     * 只判断退费成功还是失败
     * @param requestVo
     * @return 成功：返回渠道退款单号 失败：失败原因
     */
    @ApiOperationSupport(order = 1104)
    @ApiOperation(value = "1104 订单退款")
    @DynamicResponseParameters(properties = {
            @DynamicParameter(name = "retCode",value = "通讯码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCode",value = "渠道错误码", example = "SUCCESS", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "errCodeDes",value = "渠道错误描述", example = "交易成功", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchId",value = "商户号", example = "10000000", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchOrderNo",value = "商户号订单号", example = "P2010203029245", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "channelOrderNo",value = "渠道订单号", example = "102348934385486857", required = true, dataTypeClass = String.class),
            @DynamicParameter(name = "mchRefundNo",value = "商户号退款订单号", example = "R2010203029245", required = true),
            @DynamicParameter(name = "channelRefundNo",value = "渠道退款订单号", example = "24548934385486857", required = true, dataTypeClass = String.class)
    })
    @RequestMapping(value = "/refund/create_order", method = RequestMethod.POST)
    public String refundOrder(@ModelAttribute VoReqRefund requestVo, BindingResult bindingResult) {
        _log.info("###### 开始接收商户统一退款请求 ######");
        TradeResultVo tradeResultVo = new TradeResultVo();
        //公共返回
        tradeResultVo.setMchId(requestVo.getMchId());
        tradeResultVo.setMchOrderNo(requestVo.getMchOrderNo());
        tradeResultVo.setChannelOrderNo(requestVo.getChannelOrderNo());

        try {
            //参数校验
            smartPayService.validateParams(requestVo,bindingResult);
            //调用服务
            VoResTradePayment<VoResRefund> payResultVo = paymentManagerService.refund(requestVo);
            //结果转换
            tradeResultVo.setRetCode(payResultVo.getRetCode());

            VoResRefund resPayCode = payResultVo.getRetData();
            _log.info(">>>>>>>>>>>>返回：" + JSON.toJSONString(resPayCode));
            //公共渠道状态
            tradeResultVo.setErrCode(resPayCode.getErrCode());
            tradeResultVo.setErrCodeDes(resPayCode.getErrCodeDes());

            tradeResultVo.setErrCode(resPayCode.getErrCode());
            tradeResultVo.setErrCodeDes(resPayCode.getErrCodeDes());

            if (PayConstant.TRADE_STATUS_SUCCESS.equalsIgnoreCase(payResultVo.getRetCode())){
                tradeResultVo.setMchId(resPayCode.getMchId());
                tradeResultVo.setMchOrderNo(resPayCode.getMchOrderNo());
                tradeResultVo.setChannelOrderNo(resPayCode.getChannelOrderNo());
                tradeResultVo.setMchRefundNo(resPayCode.getMchRefundNo());
                tradeResultVo.setChannelRefundNo(resPayCode.getChannelRefundNo());
            }
        } catch (Exception e) {
            e.printStackTrace();
            tradeResultVo.setRetCode(PayConstant.TRADE_STATUS_FAILED);
            tradeResultVo.setErrCode(PayConstant.TRADE_STATUS_SYSTEM_ERROR_CODE);
            tradeResultVo.setErrCodeDes("系统异常：" + e.getMessage());
            _log.error(PayConstant.TRADE_STATUS_SYSTEM_ERROR_DESC,e);
        }

        String result = JSON.toJSONString(tradeResultVo);
        _log.info("打印结果：" + result);

        return result;
    }
}
