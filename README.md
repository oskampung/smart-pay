# smart-pay

#### 项目介绍
聚合支付,源自jmdhappy的xxpay项目。在其基础上精简了一些代码，保留了微信支付宝扫码支付，增加了条码支付和对账下载服务。原版用的mysql，这里数据库用jpa重写，原则上能支持各种主流数据库。

- 目前已经接入支付渠道：微信(条码支付、扫码支付)、支付宝(条码支付、扫码支付)；

【新版本开放中】 
- [源码smart-pay-plus地址](https://gitee.com/zhunian/smart-pay-plus.git) https://gitee.com/zhunian/smart-pay-plus.git
- [在线体验地址](http://122.51.99.227:8088/)
- [调用例子](https://gitee.com/zhunian/smart-pay-sample)

#### 软件架构
spring cloud


### 版本更新
***

版本 |日期 |描述
------- | ------- | -------
V1.0.0 |2019-07-08 |支持微信支付宝条码支付和扫码支付
V1.0.0 |2019-10-12 |补充建表语句及测试工具
V1.0.0 |2019-12-05 |发布正式版本v1.0
V1.0.1 |2019-12-15 |注册中心计划增加nacos（待）

### 项目结构
***
```
smart-pay
├── smart-cloud-eureka -- 注册中心
├── smart-cloud-fiance -- 对账服务
├── smart-cloud-mgr -- 配置中心
├── smart-cloud-pay -- 支付服务
├── smart-cloud-web -- 对外接口
├── smart-core-bean -- 公共模块
```

项目启动顺序：
```
smart-cloud-eureka > smart-cloud-mgr > smart-cloud-pay > smart-cloud-web > smart-cloud-fiance
```

服务器配置为：

| CPU  | 内存 | 操作系统
|---|---|---
|2核 | 8 GB | Windows 2008 R2 64位

安装的各软件对应的版本为（仅供参考）：

| 软件  | 版本 | 说明
|---|---|---
|JDK | 1.8 | spring boot 2
|ActiveMQ|  5.11.1 | 高版本也可以，如：5.14.3
|MsSQL | 2008 R2 | 其他版本没测过

#### 运行截图

![商户信息](https://images.gitee.com/uploads/images/2019/1205/222015_602813fc_535810.png "11.png")

![支付渠道](https://images.gitee.com/uploads/images/2019/1205/222138_b0743d95_535810.png "22.png")

#### 演示地址

[演示地址](http://www.zhunian.ltd:8088/)