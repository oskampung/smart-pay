package com.pku.smart;

import com.alibaba.fastjson.JSON;
import com.pku.smart.config.IIHApiConfig;
import com.pku.smart.utils.XmlUtils;
import com.pku.smart.vopackage.VoIIHResult;
import com.pku.smart.vopackage.si0057.*;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;

public class TestSI0057 {

    @Autowired
    IIHApiConfig config;

    public static void main(String[] args) throws Exception{
        printRqe();
    }

    private static void printRqe() throws Exception{
        VoReqIIH0057Param param = new VoReqIIH0057Param();
        VoReqIIH0057ParamData paramData = new VoReqIIH0057ParamData();
        paramData.setCode_pat("100000011500");
        paramData.setCode_card("");
        paramData.setCode_idnum("");
        paramData.setTimes_op("");
        paramData.setCode_hospital("1");
        paramData.setCode_enttp("01");
        param.setCode_user("00000");
        param.setCode_dep("4022");
        param.setCode_external("IIH");
        param.setData(paramData);
        String xml = XmlUtils.toXml(param);
        System.out.println(xml);

        String url = "http://10.100.254.55/iih.hewcxyy.ei.api.i.IApiService?wsdl&access_token=421a963e-2281-466d-867e-20019f9c9e52";
        String methodName = "apiEntry";
        String[] method = new String[]{"SI0057",xml};
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();    // 策略
        httpClientPolicy.setConnectionTimeout( 36000 );    //连接超时
        httpClientPolicy.setAllowChunking( false );
        httpClientPolicy.setReceiveTimeout( 10000 );       //接收超时
        Client client = dcf.createClient(url);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        http.setClient(httpClientPolicy);
        Object[] objects = client.invoke(methodName, method);
        System.out.println(JSON.toJSONString(objects));

        xml = (String) objects[0];
        Class[] clacks = new Class[]{VoIIHResult.class, VoResIIH0057Result.class, VoResIIH0057ResultData.class, VoResIIH0057ResultDataEntinfo.class};
        VoResIIH0057Result result = XmlUtils.parseFromXml(clacks, xml);
        System.out.println(JSON.toJSONString(result));
    }

    private static void printRes() {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<iihresult> \n" +
                "  <Code>0</Code>  \n" +
                "  <Msg>成功</Msg>  \n" +
                "  <Data> \n" +
                "    <Entinfos> \n" +
                "      <Entinfo> \n" +
                "        <Code_ent>2000000018</Code_ent>  \n" +
                "        <Times_op>1</Times_op>  \n" +
                "        <Code_enttp>00</Code_enttp>  \n" +
                "        <Amt_ent>2559</Amt_ent>  \n" +
                "        <Dt_acpt>2020-05-18 11:37:55</Dt_acpt>  \n" +
                "        <Note></Note>  \n" +
                "        <Presinfos> \n" +
                "          <Presinfo> \n" +
                "            <Code_apply>2000000018-R0000000368NN1</Code_apply>  \n" +
                "            <Sd_srvtp>01</Sd_srvtp>  \n" +
                "            <Name_or>0.9%氯化钠注射液</Name_or>  \n" +
                "            <Dt_effe>2020-05-18 09:00:00</Dt_effe>  \n" +
                "            <Code_dep_or>100101</Code_dep_or>  \n" +
                "            <Name_dep_or>骨一科门诊</Name_dep_or>  \n" +
                "            <Code_emp_or>00140</Code_emp_or>  \n" +
                "            <Name_emp_or>颜国飞</Name_emp_or>  \n" +
                "            <Amt_pres>759</Amt_pres>  \n" +
                "            <Presitms> \n" +
                "              <Presitm> \n" +
                "                <Code_srv>YP000002</Code_srv>  \n" +
                "                <Name_srv>0.9%氯化钠注射液</Name_srv>  \n" +
                "                <Name_srvu>瓶</Name_srvu>  \n" +
                "                <Spec>500ml/瓶</Spec>  \n" +
                "                <Price>253</Price>  \n" +
                "                <Quan>3</Quan>  \n" +
                "                <Code_dep_mp>201705</Code_dep_mp>  \n" +
                "                <Name_dep_mp>门诊西药房</Name_dep_mp>  \n" +
                "                <Amt>759</Amt>  \n" +
                "                <Dt_or>2020-05-18 09:00:00</Dt_or> \n" +
                "              </Presitm> \n" +
                "            </Presitms> \n" +
                "          </Presinfo>  \n" +
                "          <Presinfo> \n" +
                "            <Code_apply>T1001Z8100000000368NN2</Code_apply>  \n" +
                "            <Sd_srvtp>05</Sd_srvtp>  \n" +
                "            <Name_or>拔罐疗法</Name_or>  \n" +
                "            <Dt_effe>2020-05-18 15:24:58</Dt_effe>  \n" +
                "            <Code_dep_or>100101</Code_dep_or>  \n" +
                "            <Name_dep_or>骨一科门诊</Name_dep_or>  \n" +
                "            <Code_emp_or>00140</Code_emp_or>  \n" +
                "            <Name_emp_or>颜国飞</Name_emp_or>  \n" +
                "            <Amt_pres>1800</Amt_pres>  \n" +
                "            <Presitms> \n" +
                "              <Presitm> \n" +
                "                <Code_srv>44000000400</Code_srv>  \n" +
                "                <Name_srv>拔罐疗法</Name_srv>  \n" +
                "                <Name_srvu>3罐</Name_srvu>  \n" +
                "                <Spec></Spec>  \n" +
                "                <Price>900</Price>  \n" +
                "                <Quan>2</Quan>  \n" +
                "                <Code_dep_mp>100101</Code_dep_mp>  \n" +
                "                <Name_dep_mp>骨一科门诊</Name_dep_mp>  \n" +
                "                <Amt>1800</Amt>  \n" +
                "                <Dt_or>2020-05-18 15:24:58</Dt_or> \n" +
                "              </Presitm> \n" +
                "            </Presitms> \n" +
                "          </Presinfo> \n" +
                "        </Presinfos> \n" +
                "      </Entinfo> \n" +
                "    </Entinfos> \n" +
                "  </Data>\n" +
                "</iihresult>";

        Class[] clacks = new Class[]{VoIIHResult.class, VoResIIH0057Result.class, VoResIIH0057ResultData.class, VoResIIH0057ResultDataEntinfo.class};
        VoResIIH0057Result result = XmlUtils.parseFromXml(clacks, xml);
        System.out.println(JSON.toJSONString(result));
    }
}
