package com.pku.smart;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmartPayWebSampleShopApplicationTests {

    @Test
    public void contextLoads() {
    }

}
