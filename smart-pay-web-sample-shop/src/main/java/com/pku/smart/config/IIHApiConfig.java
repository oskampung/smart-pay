package com.pku.smart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix="iih.api")
public class IIHApiConfig {
    private String url;
    private String method;
    private String code_hospital;
    private String code_user;
    private String code_external;
    private String code_enttp;
}
