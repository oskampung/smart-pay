package com.pku.smart.utils;

import com.alibaba.fastjson.JSON;
import com.pku.smart.log.MyLog;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.parser.XmlTreeBuilder;

public class XmlUtils {

    private static final MyLog _log = MyLog.getLog(XmlUtils.class);

    /**
     *
     * @param clazz
     *            类的字节码文件
     * @param xml
     *            传入的XML字符串
     * @return XML字符串转实体类
     */
    public static <T> T parseFromXml(Class<T> clazz, String xml) {
        XStream xStream = new XStream(new DomDriver());
        xStream.ignoreUnknownElements();
        xStream.processAnnotations(clazz);
        @SuppressWarnings("unchecked")
        T t = (T) xStream.fromXML(xml);
        return t;
    }

    public static <T> T parseFromXml(Class[] clazz, String xml) {
        XStream xStream = new XStream(new DomDriver());
        xStream.ignoreUnknownElements();
        xStream.processAnnotations(clazz);
        xStream.autodetectAnnotations(true);
        @SuppressWarnings("unchecked")
        T t = (T) xStream.fromXML(xml);
        return t;
    }

    /**
     *
     *
     * @param obj
     *            实体类
     * @return 实体类转XML字符串
     */
    public static String toXml(Object obj) {
        XStream xStream = new XStream(new DomDriver());
        // 扫描@XStream注解
        xStream.processAnnotations(obj.getClass());
        xStream.autodetectAnnotations(true);
        xStream.aliasSystemAttribute(null, "class"); // 去掉 class 属性
        return xStream.toXML(obj).replaceAll("\\_+", "_");//正则过滤双下划线转为单下划线
    }

    /**
     *
     * @Description: 获取xml格式字符串中指定的元素标签片段
     * @param xml
     *            需要xml格式的字符串
     * @param elementId
     *            节点名
     * @return
     */
    public static String getElementString(String xml, String elementId) {
        if (StringUtils.isBlank(xml) || StringUtils.isBlank(elementId)) {
            return null;
        }
        // 获取document对象
        Document document = Jsoup.parse(xml, "", new Parser(new XmlTreeBuilder()));
        // 获取对应的document片段
        return document.select(elementId).toString().replaceAll("\\s*", "");
    }

    /**
     *
     * @Description: xml字符串节点片段转对象
     * @param xml
     * @param elementId
     * @param clazz
     * @return
     */
    public static <T> T parseElementObj(String xml, String elementId, Class<T> clazz) {
        if (StringUtils.isBlank(xml) || StringUtils.isBlank(elementId)) {
            return null;
        }
        return parseFromXml(clazz, getElementString(xml, elementId));
    }

    public static void main(String[] args) {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><MESSAGE><HEAD><N_VER>1.0</N_VER><N_MSGNO>1202</N_MSGNO><N_SENDID>100000</N_SENDID><N_SENDNM>服务平台</N_SENDNM><N_RECEID>000196</N_RECEID><N_RECENM>天津市第一中心医院</N_RECENM><N_AKB020>000196</N_AKB020><N_YKB211/></HEAD><BODY><N_MSGID>1000000000000001</N_MSGID><N_INPUT><DATA><INPUTS><INPUT/></INPUTS></DATA></N_INPUT><N_WORKDATE/><N_REFMSGID/><N_OUTPUT><DATA><OUTPUTS><OUTPUT/></OUTPUTS></DATA></N_OUTPUT><N_STATUS>0000</N_STATUS><N_ERRMSG/></BODY></MESSAGE>";
        //VoYYT_JYB_REQ base = XmlUtils.parseFromXml(VoYYT_JYB_REQ.class,xml);
        //System.out.println(JSON.toJSONString(base));
    }

}
