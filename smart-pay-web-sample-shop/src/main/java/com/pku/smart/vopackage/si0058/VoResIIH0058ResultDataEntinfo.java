package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

@Data
@XStreamAlias("Entinfo")
public class VoResIIH0058ResultDataEntinfo {
    private String Code_ent;//<Code_ent>2000000018</Code_ent>
    private String Times_op;//<Times_op>1</Times_op>
    private String Code_enttp;//<Code_enttp>00</Code_enttp>
    private String Amt_ent;//<Amt_ent>2559</Amt_ent>
    private String Dt_acpt;//<Dt_acpt>2020-05-18 11:37:55</Dt_acpt>
    private String Note;//<Note></Note>
    @XStreamAlias("Presinfos")
    private VoResIIH0058ResultDataEntinfoPresinfoList Presinfos;
}
