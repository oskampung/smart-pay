package com.pku.smart.vopackage.si0057;

import com.pku.smart.vopackage.VoIIHParam;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("iihparam")
public class VoReqIIH0057Param extends VoIIHParam implements Serializable {
    @XStreamAlias("Data")
    private VoReqIIH0057ParamData data = new VoReqIIH0057ParamData();
}
