package com.pku.smart.vopackage.si0070;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@XStreamAlias("Hpinfos")
public class VoReqIIH0070ParamDataHpinfoList implements Serializable {
    @XStreamImplicit(itemFieldName = "Hpinfo")
    private List<VoReqIIH0070ParamDataHpinfo> Entinfos = new ArrayList<>();
}
