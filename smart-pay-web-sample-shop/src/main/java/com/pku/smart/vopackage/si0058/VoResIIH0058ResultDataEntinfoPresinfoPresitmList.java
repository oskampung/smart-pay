package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@XStreamAlias("Presitms")
public class VoResIIH0058ResultDataEntinfoPresinfoPresitmList implements Serializable {
    @XStreamImplicit(itemFieldName = "Presitm")
    private List<VoResIIH0058ResultDataEntinfoPresinfoPresitm> Presitm;
}
