package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Data")
public class VoResIIH0058ResultData implements Serializable {
    private String Payno;
    @XStreamAlias("Entinfos")
    private VoResIIH0058ResultDataEntinfoList Entinfos;
}
