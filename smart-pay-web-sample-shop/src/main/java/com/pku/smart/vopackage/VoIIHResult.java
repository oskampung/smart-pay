package com.pku.smart.vopackage;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("iihresult")
public class VoIIHResult implements Serializable {
    //<Code>0</Code>
    @XStreamAlias("Code")
    private String Code;
    //<Msg>成功</Msg>
    @XStreamAlias("Msg")
    private String Msg;
}
