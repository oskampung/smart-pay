package com.pku.smart.vopackage.si0070;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Hpinfo")
public class VoReqIIH0070ParamDataHpinfo implements Serializable {
    //<!--医保读卡信息-->
    private String Hpcardinfo = "";//<Hpcardinfo></Hpcardinfo>
    //<!--医保登记信息-->
    private String Hpregisterrtninfo = "";//<Hpregisterrtninfo></Hpregisterrtninfo>
    //<!--医保明细信息-->
    private String Hpdetailuploadrtninfo = "";//<Hpdetailuploadrtninfo></Hpdetailuploadrtninfo>
    //<!--医保结算信息-->
    private String Hpstrtninfo = "";//<Hpstrtninfo></Hpstrtninfo>
    //<!--交易业务编码-->
    private String Busicode = "";//<Busicode></Busicode>
    //<!--医保结算入参信息-->
    private String Hpstinfo = "";//<Hpstinfo></Hpstinfo>
}
