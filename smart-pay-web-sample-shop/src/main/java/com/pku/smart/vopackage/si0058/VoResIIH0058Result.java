package com.pku.smart.vopackage.si0058;

import com.pku.smart.vopackage.VoIIHResult;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("iihresult")
public class VoResIIH0058Result extends VoIIHResult implements Serializable {
    @XStreamAlias("Data")
    private VoResIIH0058ResultData Data = new VoResIIH0058ResultData();
    @XStreamAlias("DataReq")
    private VoReqIIH0058ParamData DataReq = new VoReqIIH0058ParamData();
}
