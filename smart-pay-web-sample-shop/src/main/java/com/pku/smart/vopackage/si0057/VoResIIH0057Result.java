package com.pku.smart.vopackage.si0057;

import com.pku.smart.vopackage.VoIIHResult;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("iihresult")
public class VoResIIH0057Result extends VoIIHResult implements Serializable {
    @XStreamAlias("Data")
    private VoResIIH0057ResultData Data = new VoResIIH0057ResultData();
}
