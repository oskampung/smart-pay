package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@XStreamAlias("Presinfos")
public class VoReqIIH0058ParamDataEntinfoPresinfoList implements Serializable {
    @XStreamImplicit(itemFieldName = "Presinfo")
    private List<VoReqIIH0058ParamDataEntinfoPresinfo> Presinfos = new ArrayList<>();
}
