package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@XStreamAlias("Presitms")
public class VoReqIIH0058ParamDataEntinfoPresinfoPresitmList implements Serializable {
    @XStreamImplicit(itemFieldName = "Presitm")
    private List<VoReqIIH0058ParamDataEntinfoPresinfoPresitm> Presitm = new ArrayList<>();
}
