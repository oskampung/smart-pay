package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Presitm")
public class VoResIIH0058ResultDataEntinfoPresinfoPresitm implements Serializable {
    private String Code_srv;//<Code_srv>YP000002</Code_srv>
    private String Name_srv;//<Name_srv>0.9%氯化钠注射液</Name_srv>
    private String Name_srvu;//<Name_srvu>瓶</Name_srvu>
    private String Spec;//<Spec>500ml/瓶</Spec>
    private String Price;//<Price>253</Price>
    private String Quan;//<Quan>3</Quan>
    private String Code_dep_mp;//<Code_dep_mp>201705</Code_dep_mp>
    private String Name_dep_mp;//<Name_dep_mp>门诊西药房</Name_dep_mp>
    private String Amt;//<Amt>759</Amt>
    private String Dt_or;//<Dt_or>2020-05-18 09:00:00</Dt_or>
}
