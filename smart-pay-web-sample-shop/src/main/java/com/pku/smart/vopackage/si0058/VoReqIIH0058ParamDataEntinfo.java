package com.pku.smart.vopackage.si0058;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Entinfo")
public class VoReqIIH0058ParamDataEntinfo implements Serializable {
    //<!--就诊编码-->
    private String Code_ent = "";//<Code_ent>2000000018</Code_ent>
    //<!--门诊就诊次数-->
    private String Times_op = "";//<Times_op>1</Times_op>
    //<!--门诊就诊类型-->
    private String Code_enttp = "";//<Code_enttp>00</Code_enttp>
    //<!--患者类型-->
    private String Name_paticate = "";//<Name_paticate></Name_paticate>
    //<!--本次就诊金额-->
    private String Amt_ent = "";//<Amt_ent></Amt_ent>
    //<!--门诊接诊时间-->
    private String Dt_acpt = "";//<Dt_acpt></Dt_acpt>
    //<!--备注-->
    private String Note = "";//<Note></Note>
    //<!--门诊划价处方信息-->
    @XStreamAlias("Presinfos")
    private VoReqIIH0058ParamDataEntinfoPresinfoList Presinfos = new VoReqIIH0058ParamDataEntinfoPresinfoList();
}
