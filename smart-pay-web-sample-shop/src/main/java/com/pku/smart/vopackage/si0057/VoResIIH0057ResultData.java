package com.pku.smart.vopackage.si0057;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("Data")
public class VoResIIH0057ResultData implements Serializable {
    @XStreamAlias("Entinfos")
    private VoResIIH0057ResultDataEntinfoList Entinfos = new VoResIIH0057ResultDataEntinfoList();
}
