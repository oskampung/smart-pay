package com.pku.smart.service;

import com.alibaba.fastjson.JSON;
import com.pku.smart.config.IIHApiConfig;
import com.pku.smart.log.MyLog;
import com.pku.smart.utils.XmlUtils;
import com.pku.smart.vopackage.VoIIHResult;
import com.pku.smart.vopackage.si0057.*;
import com.pku.smart.vopackage.si0058.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SI0058Service {

    private static final MyLog _log = MyLog.getLog(SI0058Service.class);

    @Autowired
    IIHApiConfig config;

    @Autowired
    SmartPayService service;

    public VoResIIH0058Result process(VoReqIIH0057Param reqIIH0057Param, VoResIIH0057Result param0057) {
        VoResIIH0058Result resultData = new VoResIIH0058Result();

        VoReqIIH0058Param param = new VoReqIIH0058Param();
        param.setCode_user(config.getCode_user());
        param.setCode_dep(reqIIH0057Param.getCode_dep());
        param.setCode_external(config.getCode_external());
        param.getData().setCode_hospital(config.getCode_hospital());
        param.getData().setCode_pat(reqIIH0057Param.getData().getCode_pat());
        param.getData().setCode_enttp(reqIIH0057Param.getData().getCode_enttp());
        param.getData().setCode_opera(config.getCode_user());

        List<VoResIIH0057ResultDataEntinfo> iih0057ResultDataEntinfoList = param0057.getData().getEntinfos().getEntinfo();

        List<VoReqIIH0058ParamDataEntinfo> iih0058ParamDataEntinfoList = param.getData().getEntinfos().getEntinfos();
        iih0057ResultDataEntinfoList.forEach(x -> {
            VoReqIIH0058ParamDataEntinfo iih0058ParamDataEntinfo = new VoReqIIH0058ParamDataEntinfo();
            iih0058ParamDataEntinfo.setCode_ent(x.getCode_ent());
            iih0058ParamDataEntinfo.setTimes_op(x.getTimes_op());
            iih0058ParamDataEntinfo.setCode_enttp(x.getCode_enttp());
            iih0058ParamDataEntinfo.setName_paticate("");
            iih0058ParamDataEntinfo.setAmt_ent(x.getAmt_ent());
            iih0058ParamDataEntinfo.setDt_acpt(x.getDt_acpt());
            iih0058ParamDataEntinfo.setNote(x.getNote());

            List<VoReqIIH0058ParamDataEntinfoPresinfo> iih0058ParamDataEntinfoPresinfoList = iih0058ParamDataEntinfo.getPresinfos().getPresinfos();
            x.getPresinfos().getPresinfo().forEach(xx -> {
                VoReqIIH0058ParamDataEntinfoPresinfo iih0058ParamDataEntinfoPresinfo = new VoReqIIH0058ParamDataEntinfoPresinfo();
                iih0058ParamDataEntinfoPresinfo.setCode_apply(xx.getCode_apply());
                iih0058ParamDataEntinfoPresinfo.setSd_srvtp(xx.getSd_srvtp());
                iih0058ParamDataEntinfoPresinfo.setName_or(xx.getName_or());
                iih0058ParamDataEntinfoPresinfo.setDt_effe(xx.getDt_effe());
                iih0058ParamDataEntinfoPresinfo.setCode_dep_or(xx.getCode_dep_or());
                iih0058ParamDataEntinfoPresinfo.setCode_emp_or(xx.getCode_emp_or());
                iih0058ParamDataEntinfoPresinfo.setName_dep_or(xx.getName_dep_or());
                iih0058ParamDataEntinfoPresinfo.setName_emp_or(xx.getName_emp_or());
                iih0058ParamDataEntinfoPresinfo.setAmt_pres(xx.getAmt_pres());

                List<VoReqIIH0058ParamDataEntinfoPresinfoPresitm> iih0058ParamDataEntinfoPresinfoPresitmList = iih0058ParamDataEntinfoPresinfo.getPresitms().getPresitm();
                xx.getPresitms().getPresitm().forEach(xxx->{
                    VoReqIIH0058ParamDataEntinfoPresinfoPresitm voReqIIH0058ParamDataEntinfoPresinfoPresitm = new VoReqIIH0058ParamDataEntinfoPresinfoPresitm();
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setCode_srv(xxx.getCode_srv());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setName_srv(xxx.getName_srv());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setName_srvu(xxx.getName_srvu());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setSpec(xxx.getSpec());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setPrice(xxx.getPrice());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setQuan(xxx.getQuan());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setCode_dep_mp(xxx.getCode_dep_mp());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setName_dep_mp(xxx.getName_dep_mp());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setAmt(xxx.getAmt());
                    voReqIIH0058ParamDataEntinfoPresinfoPresitm.setDt_or(xxx.getDt_or());
                    iih0058ParamDataEntinfoPresinfoPresitmList.add(voReqIIH0058ParamDataEntinfoPresinfoPresitm);
                });

                iih0058ParamDataEntinfoPresinfoList.add(iih0058ParamDataEntinfoPresinfo);
            });

            iih0058ParamDataEntinfoList.add(iih0058ParamDataEntinfo);
        });
        _log.info("组织参数：" + JSON.toJSONString(param));
        String xml = XmlUtils.toXml(param);
        _log.info("转换为XML：" + xml);
        String methodName = config.getMethod();
        String[] params = new String[]{"SI0058", xml};

        try {
            Object[] objects = service.invoke(methodName, params);
            _log.info("远程调用返回：" + JSON.toJSONString(objects));
            xml = (String) objects[0];
            Class[] clacks = new Class[]{VoIIHResult.class, VoResIIH0058Result.class, VoResIIH0058ResultData.class, VoResIIH0058ResultDataEntinfo.class};
            resultData = XmlUtils.parseFromXml(clacks, xml);
            resultData.setDataReq(param.getData());//参数给他
        } catch (Exception e) {
            e.printStackTrace();
            resultData.setCode("1001");
            resultData.setMsg("远程服务调用失败：" + e.getMessage());
        }

        return resultData;
    }
}
