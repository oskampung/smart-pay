package com.pku.smart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartPayWebSampleShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartPayWebSampleShopApplication.class, args);
    }

}
